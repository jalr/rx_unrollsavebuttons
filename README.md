TYPO3 CMS save button unrolling
===============================

This extension for TYPO3 CMS 7+ unrolls the dropdown of save buttons into
a list of buttons next to each other for faster access.

Configuration
-------------

There is a **User TSconfig** settings to configure this extension:

```
options.txunrollsavebuttons.order
```

This can be used to reorder the buttons. Buttons are ordered by their name.
The expected configuration is an array with before and after ordering instructions.

For instance: 

```
options.txunrollsavebuttons.order {
	_saveandclosedok {
		before = _savedok
	}
	_savedokandnew {
		after = _savedok, _saveandclosedok
	}
}
```

Typical button names are ``_savedok, _saveandclosedok, _savedokview, _savedoknew``
